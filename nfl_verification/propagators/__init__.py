from .ClosedLoopPropagator import ClosedLoopPropagator
from .ClosedLoopCROWNIBPCodebasePropagator import (
    ClosedLoopCROWNIBPCodebasePropagator,
    ClosedLoopCROWNLPPropagator,
    ClosedLoopIBPPropagator,
    ClosedLoopCROWNPropagator,
    ClosedLoopCROWNNStepPropagator,
    ClosedLoopFastLinPropagator,
)
from .ClosedLoopJaxVerifyPropagator import (
    ClosedLoopJaxPropagator,
    ClosedLoopJaxIterativePropagator,
    ClosedLoopJaxUnrolledPropagator,
    ClosedLoopJaxUnrolledJittedPropagator,
    ClosedLoopJaxPolytopePropagator,
    ClosedLoopJaxRectanglePropagator,
    ClosedLoopJaxLPPropagator,
    ClosedLoopJaxPolytopeJittedPropagator,
    ClosedLoopJaxRectangleJittedPropagator,
    ClosedLoopJaxLPJittedPropagator,
)

propagator_dict = {
    "TorchBackwardCROWNReachLP": ClosedLoopCROWNPropagator,
    "TorchBackwardCROWNNStep": ClosedLoopCROWNNStepPropagator,
    "TorchCROWNLP": ClosedLoopCROWNLPPropagator,
    "TorchIBP": ClosedLoopIBPPropagator,
    "TorchFastLin": ClosedLoopFastLinPropagator,
    "JaxIterative": ClosedLoopJaxIterativePropagator,
    "JaxUnrolled": ClosedLoopJaxUnrolledPropagator,
    "JaxUnrolledJitted": ClosedLoopJaxUnrolledJittedPropagator,
    "JaxPolytope": ClosedLoopJaxPolytopePropagator,
    "JaxRectangle": ClosedLoopJaxRectanglePropagator,
    "JaxLP": ClosedLoopJaxLPPropagator,
    "JaxPolytopeJitted": ClosedLoopJaxPolytopeJittedPropagator,
    "JaxRectangleJitted": ClosedLoopJaxRectangleJittedPropagator,
    "JaxLPJitted": ClosedLoopJaxLPJittedPropagator,
}

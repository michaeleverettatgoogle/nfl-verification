import numpy as np
from matplotlib.patches import Rectangle
from nfl_verification.utils.plot_rect_prism import rect_prism
import pypoman
from jax import tree_util
import collections
import jax_verify
import jax
import jax.numpy as jnp


class Constraint:
    def __init__(self):
        pass

class PolytopeConstraint(Constraint):
    def __init__(self, A=None, b=None):
        Constraint.__init__(self)
        self.A = A
        self.b = b

    def to_linf(self):
        if isinstance(self.A, list):
            # Mainly for backreachability, return a list of ranges if
            # the constraint contains a list of polytopes
            ranges = []
            for i in range(len(self.A)):
                vertices = np.stack(
                    pypoman.duality.compute_polytope_vertices(self.A[i], self.b[i])
                )
                ranges.append(np.dstack(
                    [np.min(vertices, axis=0), np.max(vertices, axis=0)]
                )[0])
        else:
            vertices = np.stack(
                pypoman.duality.compute_polytope_vertices(self.A, self.b)
            )
            ranges = np.dstack(
                [np.min(vertices, axis=0), np.max(vertices, axis=0)]
            )[0]
        return ranges

    def plot(self, ax, dims, color, fc_color="None", linewidth=1.5, label=None, zorder=2, plot_2d=True, ls='-'):
        if not plot_2d:
            raise NotImplementedError
            return self.plot3d(ax, dims, color, fc_color=fc_color, linewidth=linewidth, zorder=zorder)

        # TODO: this doesn't use the computed input_dims...

        if linewidth != 2.5:
            linewidth = 1.5

        lines = []

        if isinstance(self.A, list):
            # Backward reachability
            # input_constraint.A will be a list
            # of polytope facets, whose union is the estimated
            # backprojection set

            for i in range(len(self.A)):
                line = make_polytope_from_arrs(ax, self.A[i], self.b[i], color, label, zorder, ls, linewidth)
                lines += line

        else:
            # Forward reachability
            if isinstance(self.b, (np.ndarray,
                                   jnp.ndarray)) and self.b.ndim == 1:
                line = make_polytope_from_arrs(ax, self.A, self.b, color, label, zorder, ls, linewidth)
                lines += line
            else:
                for i in range(len(self.b)):
                    line = make_polytope_from_arrs(ax, self.A, self.b[i], color, label, zorder, ls, linewidth)
                    lines += line

        return lines

    def get_t_max(self):
        return len(self.b)
    
    def to_jittable(self):
        return JittablePolytopeConstraint(self.A, self.b,
                                {jax_verify.IntervalBound: None}, {})
    
    @classmethod
    def from_jittable(cls, jittable_constraint):
        return cls(
            range=np.array([jittable_constraint.lower, jittable_constraint.upper]))

    def _tree_flatten(self):
        children = (self.A, self.b)  # arrays / dynamic values
        aux_data = {}  # static values
        return (children, aux_data)

    @classmethod
    def _tree_unflatten(cls, aux_data, children):
        return cls(*children, **aux_data)


class LpConstraint(Constraint):
    def __init__(self, range=None, p=np.inf):
        Constraint.__init__(self)
        self.range = range
        self.p = p

    def plot(self, ax, dims, color, fc_color="None", linewidth=3, zorder=2, plot_2d=True, ls='-'):
        patches = []
        if not plot_2d:
            return self.plot3d(ax, dims, color, fc_color=fc_color, linewidth=linewidth, zorder=zorder)
        if isinstance(self.range, list) or (
                isinstance(self.range, (np.ndarray,
                                        jnp.ndarray)) and self.range.ndim == 3):
            for i in range(len(self.range)):
                rect = make_rect_from_arr(self.range[i], dims, color, linewidth, fc_color, ls, zorder=zorder)
                ax.add_patch(rect)
                patches.append(rect)
        elif isinstance(self.range, (np.ndarray,
                                     jnp.ndarray)) and self.range.ndim == 4:
            num_agents, num_timesteps, num_states, _ = self.range.shape
            for agent_ind in range(num_agents):
                for t in range(num_timesteps):
                    rect = make_rect_from_arr(self.range[agent_ind, t], dims, color, linewidth, fc_color, ls, zorder=zorder)
                    ax.add_patch(rect)
                    patches.append(rect)
        else:
            rect = make_rect_from_arr(self.range, dims, color, linewidth, fc_color, ls, zorder=zorder)
            ax.add_patch(rect)
            patches.append(rect)
        return patches
    
    def plot3d(self, ax, dims, color, fc_color="None", linewidth=1, zorder=2, plot_2d=True):
        patches = []
        if isinstance(self.range, list) or (
                isinstance(self.range, (np.ndarray,
                                        jnp.ndarray)) and self.range.ndim == 3):
            for i in range(len(self.range)):
                rect = rect_prism(*self.range[i][dims, :], ax, color, linewidth, fc_color, zorder=zorder)
                patches.append(rect)
        elif isinstance(self.range, (np.ndarray,
                                     jnp.ndarray)) and self.range.ndim == 4:
            num_agents, num_timesteps, num_states, _ = self.range.shape
            for agent_ind in range(num_agents):
                for t in range(num_timesteps):
                    rect = rect_prism(*self.range[agent_ind][t][dims, :], ax, color, linewidth, fc_color, zorder=zorder)
                    patches.append(rect)
        else:
            rect = rect_prism(*self.range[dims, :], ax, color, linewidth, fc_color, zorder=zorder)
            patches.append(rect)
        return patches[0]

    def get_t_max(self):
        if hasattr(self.range, 'ndim') and self.range.ndim == 4:
            return self.range.shape[1]
        return len(self.range)
    
    def to_jittable(self):
        return JittableLpConstraint(self.range[..., 0], self.range[..., 1],
                                {jax_verify.IntervalBound: None}, {})
    
    @classmethod
    def from_jittable(cls, jittable_constraint):
        return cls(
            range=np.array([jittable_constraint.lower, jittable_constraint.upper]))

    def _tree_flatten(self):
        children = (self.range,)  # arrays / dynamic values
        aux_data = {}  # static values
        return (children, aux_data)

    @classmethod
    def _tree_unflatten(cls, aux_data, children):
        return cls(*children, **aux_data)

JittableLpConstraint = collections.namedtuple(
    'JittableLpConstraint', ['lower', 'upper', 'bound_type', 'kwargs'])


def unjit_lp_constraints(*inputs):
  """Replace all the jittable bounds by standard bound objects."""
  is_jittable_constraint = lambda b: isinstance(b, JittableLpConstraint)
  unjit_bound = lambda b: next(iter(b.bound_type)).from_jittable(b)
  return jax.tree_util.tree_map(
      lambda b: unjit_bound(b) if is_jittable_constraint(b) else b,
      inputs, is_leaf=is_jittable_constraint)

tree_util.register_pytree_node(LpConstraint,
                               LpConstraint._tree_flatten,
                               LpConstraint._tree_unflatten)

JittablePolytopeConstraint = collections.namedtuple(
    'JittablePolytopeConstraint', ['A', 'b', 'bound_type', 'kwargs'])


def unjit_polytope_constraints(*inputs):
  """Replace all the jittable bounds by standard bound objects."""
  is_jittable_constraint = lambda b: isinstance(b, JittablePolytopeConstraint)
  unjit_bound = lambda b: next(iter(b.bound_type)).from_jittable(b)
  return jax.tree_util.tree_map(
      lambda b: unjit_bound(b) if is_jittable_constraint(b) else b,
      inputs, is_leaf=is_jittable_constraint)

tree_util.register_pytree_node(PolytopeConstraint,
                               PolytopeConstraint._tree_flatten,
                               PolytopeConstraint._tree_unflatten)

def make_rect_from_arr(arr, dims, color, linewidth, fc_color, ls, zorder=None):
    rect = Rectangle(
        arr[dims, 0],
        arr[dims[0], 1]
        - arr[dims[0], 0],
        arr[dims[1], 1]
        - arr[dims[1], 0],
        fc=fc_color,
        linewidth=linewidth,
        edgecolor=color,
        zorder=zorder,
        linestyle=ls,
    )
    return rect


def make_polytope_from_arrs(ax, A, b, color, label, zorder, ls, linewidth=1.5):
    if A.shape[1] == 2:
        vertices = np.stack(
        pypoman.polygon.compute_polygon_hull(
            A, b + 1e-10
        )
    )
    else:
        dim_to_project_from = A.shape[1]
        dim_to_project_to = 2
        E = np.zeros((dim_to_project_to, dim_to_project_from))
        for i in range(dim_to_project_to):
            E[i, i] = 1
        f = np.zeros((dim_to_project_to,))
        vertices = np.unique(pypoman.projection.project_polytope((E, f), (A, b)), axis=0)
        vertices = pypoman.duality.convex_hull(vertices)

    lines = ax.plot(
        [v[0] for v in vertices] + [vertices[0][0]],
        [v[1] for v in vertices] + [vertices[0][1]],
        color=color,
        label=label,
        zorder=zorder,
        ls=ls,
        marker='x',
        linewidth=linewidth
    )
    return lines
